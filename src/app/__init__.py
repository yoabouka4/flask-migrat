from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_script import Manager

db = SQLAlchemy()

migrate = Migrate()


def create_app():
    application = Flask(__name__)
    application.config['DEBUG'] = True
    application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://christ:u08NCWmOuWdiD93b@localhost:3306/test-migrate'
    application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    from .Models import Member
    db.init_app(application)
    migrate.init_app(application, db)

    with application.app_context():
        from . import app

    return application
