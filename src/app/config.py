
class BaseConfig(object):
    DEBUG = False
    TESTING = False


class Config(BaseConfig):
    DEBUG = True
